import React from "react";


class ShoesList extends React.Component {
    constructor() {
        super()
        this.state = {
            "shoes": []
        }
        this.delete = this.delete.bind(this)
    }
    async componentDidMount() {
        const url = "http://localhost:8080/api/shoes/"
        let response = await fetch(url)

        if(response.ok) {
            let data = await response.json()
            this.setState({"shoes": data.shoes})
        }
    }
    async delete(shoe_id, event) {
        event.preventDefault();
        const url = `http://localhost:8080/api/shoes/${shoe_id}`
        const fetchConfig = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        }

        await fetch(url, fetchConfig)
        const newShoesList = this.state.shoes.filter(shoe => shoe.id !== shoe_id)
        this.setState({"shoes": newShoesList})
    }

    render() {
        return(
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Color</th>
                            <th>Bin Number</th>
                            <th>Delete?</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.shoes.map(shoe=> {
                    return (
                        <tr key={shoe.id}>
                            <td>{shoe.name}</td>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.bin.bin_number}</td>
                            <td>
                                <form>
                                    <button onClick={(event) => this.delete(shoe.id, event)}>Delete</button>
                                </form>
                            </td>
                        </tr>
                    )
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ShoesList